[settings]
    arch=x86_64
    build_type=Debug
    compiler=Visual Studio
    compiler.runtime=MDd
    compiler.version=16
    os=Windows

[requires]
    Hello/0.2

[options]


[full_settings]
    arch=x86_64
    build_type=Debug
    compiler=Visual Studio
    compiler.runtime=MDd
    compiler.toolset=v142
    compiler.version=16
    os=Windows

[full_requires]
    Hello/0.2@memsharded/testing:d057732059ea44a47760900cb5e4855d2bea8714

[full_options]


[recipe_hash]


[env]

